# sensor-diodes
Project that I've made in high school for my final high school project in year 2015. 

thermometer with screen made from LED's

This is example of simple arduino project in which you will create temperature sensor with your own segment display.

[![YOUTUBE VIEO](https://raw.githubusercontent.com/paupav/pic/master/slika-zavrsni.png)](https://www.youtube.com/watch?v=epclVPG0HZQ)

[Click here for youtube video](https://www.youtube.com/watch?v=epclVPG0HZQ)


#### Required equipment 
- 38 LED's
- lm35 temperature sensor
- 220 Ohm resistor
- jump wires
- breadboard

#### Optional equipment
- atmega 328-pu (optional)
- 5V power suply
- perforated board
- crystal oscilator 16 MHz
- two 22pF capacitors
- solder


#### How to

- connect screen as shown on the scheme ( numbers at the picture bellow are arduino pins ) while making sure that they form number 8 
- connect the part that forms °C to the +5 V and GND
![alt tag](https://raw.githubusercontent.com/paupav/pic/master/shema-zavrsni.png)
![alt tag](https://github.com/paupav/pic/blob/master/izgled-broja.png)


- upload sketch downloaded from https://github.com/paupav/sensor-diodes to your arduino

#### Scheme with atmega 328-pu (adittional step)

- scheme for atmega 328-pu on soldering board
![alt tag](https://raw.githubusercontent.com/paupav/pic/master/shema-zavrsni-cijela.png)

